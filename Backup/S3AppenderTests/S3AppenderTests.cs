﻿using System;
using System.Threading;

using log4net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace S3AppenderTests
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class S3AppenderTests
	{
		private static ILog log;

		public S3AppenderTests ( )
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[ClassInitialize()]
		[DeploymentItem( "S3AppenderTests\\log4net.config" )]
		public static void MyClassInitialize ( TestContext testContext )
		{
			log4net.Config.XmlConfigurator.ConfigureAndWatch( new System.IO.FileInfo( "log4net.config" ) );
			log = LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );
		}

		[TestMethod]
		[DeploymentItem( "S3AppenderTests\\log4net.config" )]
		public void TestThatFileRollsAndIsSentToS3BasedOnTime ( )
		{
			// this should create 5 files on S3 since we are rolling every minute

			for ( int i = 0; i < 5; i++ )
			{
				log.Debug( "test debug message" );
				log.Info( "test info message" );
				log.Warn( "test warn message" );
				log.Error( "test error", new Exception( "fake exception" ) );
				log.Fatal( "test fatal", new Exception( "fake exception" ) );
				Thread.Sleep( 60001 );
			}
		}

		[TestMethod]
		[DeploymentItem( "S3AppenderTests\\log4net.config" )]
		public void TestThatFileRollsAndIsSentToS3BasedOnSize ( )
		{
			//run this loop 10 times to generate 10 files
			for ( int i = 0; i < 10; i++ )
			{
				// this should create a file on S3 since we are rolling every 100KB and the total amout of content for each loop is ~1KB and we run the loop 100 times
				for ( int j = 0; j < 100; j++ )
				{
					log.Debug( "loop" + i + ":iteration" + j + ": test debug message - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" );
					log.Info( "loop" + i + ":iteration" + j + ": test info message - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" );
					log.Warn( "loop" + i + ":iteration" + j + ": test warn message - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" );
					log.Error( "loop" + i + ":iteration" + j + ": test error - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", new Exception( "fake exception" ) );
					log.Fatal( "loop" + i + ":iteration" + j + ": test fatal - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", new Exception( "fake exception" ) );
				}
			}

		}
	}
}
