﻿namespace ClassLibrary1
{
    using System;
    using System.Threading;
    using NUnit.Framework;
    using log4net;

    [TestFixture]
    public class S3AppenderTests
    {
        private ILog log;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("log4net.config"));
            log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        [Test]
        public void TestThatFileRollsAndIsSentToS3BasedOnTime()
        {
            // this should create 5 files on S3 since we are rolling every minute

            for (int i = 0; i < 5; i++)
            {
                log.Debug("test debug message");
                log.Info("test info message");
                log.Warn("test warn message");
                log.Error("test error", new Exception("fake exception"));
                log.Fatal("test fatal", new Exception("fake exception"));
                Thread.Sleep(60001);
            }
        }

        [Test]
        public void TestThatFileRollsAndIsSentToS3BasedOnSize()
        {
            //run this loop 10 times to generate 10 files
            for (int i = 0; i < 10; i++)
            {
                // this should create a file on S3 since we are rolling every 100KB and the total amout of content for each loop is ~1KB and we run the loop 100 times
                for (int j = 0; j < 100; j++)
                {
                    log.Debug("loop" + i + ":iteration" + j + ": test debug message - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
                    log.Info("loop" + i + ":iteration" + j + ": test info message - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
                    log.Warn("loop" + i + ":iteration" + j + ": test warn message - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
                    log.Error("loop" + i + ":iteration" + j + ": test error - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", new Exception("fake exception"));
                    log.Fatal("loop" + i + ":iteration" + j + ": test fatal - 01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", new Exception("fake exception"));
                }
            }
        }
    }
}